<?php
/**
 * Created by solly [15.06.16 21:05]
 */

namespace core\widgets;


use dosamigos\selectize\SelectizeTextInput;

class TagFormWidget extends SelectizeTextInput
{
    public $options = ['class' => 'form-control'];
    public $loadUrl = ['tags/list'];
    public $clientOptions
        = [
            'plugins' => ['remove_button'],
            'valueField' => 'name',
            'labelField' => 'name',
            'searchField' => ['name'],
            'create' => true,
        ];
}