Widget Collection
=================
TagFormWidget - wrapper around dosamigos\selectize\SelectizeTextInput with custom style
grid\ActionColumn - wrapper around \yii\grid\ActionColumn with styled default buttons
grid\PositionColumn add up and down buttons for change item order
grid\LongColumn - column for long data with smart collapse integration
grid\RestoreColumn - column for restore soft-deleted item action