<?php
/**
 * Created by solly [12.06.16 11:32]
 */

namespace core\widgets\grid;

use yii\bootstrap\Collapse;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;


/**
 * Class LongColumn - column with smart-collapsible data
 *
 * @package core\widgets\grid
 */
class LongColumn extends DataColumn
{

    /**
     * Wrap content into pre tag
     *
     * @var bool $preWrap
     **/
    public $preWrap = true;

    /**
     * @var int $maxchars - max symbols without collapsing
     */
    public $maxchars = 255;
    /**
     * @var array
     */
    public $headerOptions = ['class' => 'col-xs-6 col-md-6',];
    /**
     * @var array
     */
    public $contentOptions = ['style' => 'word-wrap:break-words;max-width:700px;'];
    /**
     * @var array
     */
    public $valueTagOptions
        = [
            'style' => 'word-wrap:break-words;max-width:700px;'
        ];
    /**
     * @var bool
     */
    public $filter = false;
    /**
     * @var bool $encode --- use htmlencode format
     **/
    public $encode = true;
    /**
     * @var bool convert from json to string with VarDumper::export
     **/
    public $unjson = false;

    /**
     * @var string|int|false preview value - if false - truncated original value will be used, if int - truncate
     * original for this chars, if string - setted value
     **/
    public $previewValue = '... смотреть ...';

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        if ($this->content === null) {
            $value = $this->getDataCellValue($model, $key, $index);
            if (mb_strlen($value) > $this->maxchars) {
                if($this->previewValue===false){
                    $preview = $this->truncate($this->unJson($value), $this->maxchars);
                }elseif (is_numeric($this->previewValue)){
                    $preview = $this->truncate($this->unJson($value), $this->previewValue);
                }else{
                    $preview = $this->previewValue;
                }
                $value = $this->preWrap($this->encode($this->unJson($value)));
                return Collapse::widget(
                    [
                        'items' => [
                            ['label' => $preview, 'content' => $value, 'options' => $this->valueTagOptions]
                        ],
                        'encodeLabels' => true,
                    ]
                );
            } else {
                return $this->preWrap($this->encode($this->unJson($value)));
            }
        } else {
            return parent::renderDataCellContent($model, $key, $index);
        }
    }

    /**
     * @param $value
     * @param $max
     *
     * @return string
     */
    protected function truncate($value, $max)
    {
        return StringHelper::truncate($value, $max);
    }

    /**
     * @param $value
     *
     * @return mixed|string
     */
    protected function unJson($value)
    {
        if ($this->unjson && is_string($value)) {
            try {
                $value = Json::decode($value);
                return VarDumper::export($value);
            } catch (\Exception $e) {
                return $value;
            }
        }
        if (is_array($value) || is_object($value)) {
            return VarDumper::export($value);
        }
        return $value;
    }

    /**
     * @param $value
     *
     * @return string
     */
    protected function preWrap($value)
    {
        return Html::tag('pre', $value);
    }

    /**
     * @param $value
     *
     * @return string
     */
    protected function encode($value)
    {
        if ($this->encode) {
            return Html::encode($value);
        }
        return $value;
    }
}