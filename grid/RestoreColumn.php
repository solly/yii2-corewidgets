<?php
/**
 * Created by solly [10.08.16 0:18]
 */

namespace core\widgets\grid;


use yii\grid\ActionColumn;
use yii\helpers\Html;

/**
 * Class RestoreColumn
 *
 * @package core\widgets\grid
 */
class RestoreColumn extends ActionColumn
{

    /**
     *
     */
    public function initDefaultButtons()
    {
        if (!isset($this->buttons['restore'])) {
            $this->buttons['restore'] = function ($url, $model, $key) {
                $options = array_merge(
                    [
                        'title' => 'Восстановить',
                        'data-toggle' => 'tooltip',
                        'class' => 'btn btn-sm btn-info',
                        'aria-label' => 'Восстановить',
                        'data-pjax' => '0',
                    ],
                    $this->buttonOptions
                );
                return Html::a(
                    '<i class="fa fa-leaf"></i> ',
                    $this->createUrl('restore', $model, $key, null),
                    $options
                );
            };
        }
    }
}