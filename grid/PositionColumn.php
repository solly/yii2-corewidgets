<?php
/**
 * Created by solly [12.06.16 15:41]
 */

namespace core\widgets\grid;


use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;

class PositionColumn extends ActionColumn
{
    const SORT_UP = 'up';
    const SORT_DOWN = 'down';
    public $template = '{up} {down}';

    public $sortRoute = 'sort';
    public $directionParam = 'direction';
    public $sortStep = 5;
    public $stepParam = 'step';

    public $positionAttribute = 'position';

    public function init()
    {
        parent::init();
    }

    /**
     * Initializes the default button rendering callbacks.
     */
    protected function initDefaultButtons()
    {
        if (!isset($this->buttons['up'])) {
            $this->buttons['up'] = function ($url, $model, $key) {
                $options = array_merge(
                    [
                        'title' => 'Уменьшить позицию',
                        'aria-label' => 'Выше',
                        'data-toggle' => 'tooltip',
                        'data-method' => 'post',
                        'class' => 'btn btn-sm btn-default sort-button'
                        //'data-pjax' => '0',
                    ],
                    $this->buttonOptions
                );
                $url = Url::to(
                    [
                        $this->sortRoute,
                        $this->directionParam => self::SORT_DOWN,
                        $this->stepParam => $this->sortStep,
                        'id' => (string)$key
                    ]
                );
                return Html::a('<i class="fa fa-arrow-up"></i>', $url, $options) . '<span class="label label-primary">'
                . $model->{$this->positionAttribute} . '</span>';
            };
        }
        if (!isset($this->buttons['down'])) {
            $this->buttons['down'] = function ($url, $model, $key) {
                $options = array_merge(
                    [
                        'title' => 'Увеличить позицию',
                        'aria-label' => 'Ниже',
                        'data-toggle' => 'tooltip',
                        'data-method' => 'post',
                        'class' => 'btn btn-sm btn-default sort-button'
                        //'data-pjax' => '0',
                    ],
                    $this->buttonOptions
                );
                $url = Url::to(
                    [
                        $this->sortRoute,
                        $this->directionParam => self::SORT_UP,
                        $this->stepParam => $this->sortStep,
                        'id' => (string)$key
                    ]
                );
                return Html::a('<i class="fa fa-arrow-down"></i>', $url, $options);
            };
        }

    }


}