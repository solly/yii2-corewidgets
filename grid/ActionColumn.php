<?php
/**
 * Created by solly [12.06.16 15:32]
 */

namespace core\widgets\grid;


use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class ActionColumn
 * Override default with styled buttons
 *
 * @package core\widgets\grid
 */
class ActionColumn extends \yii\grid\ActionColumn
{
    /**
     * @var array
     */
    public $buttonOptions
        = [
            'data-toggle' => 'tooltip',
            'class' => 'btn btn-sm btn-default'
        ];

    /**
     * Initializes the default button rendering callbacks.
     */
    protected function initDefaultButtons()
    {
        if (!isset($this->buttons['view'])) {
            $this->buttons['view'] = function ($url, $model, $key) {
                $options = array_merge(
                    [
                        'title' => Yii::t('yii', 'View'),
                        'aria-label' => Yii::t('yii', 'View'),
                        'data-pjax' => '0',
                    ],
                    $this->buttonOptions
                );
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['update'])) {
            $this->buttons['update'] = function ($url, $model, $key) {
                $options = array_merge(
                    [
                        'title' => Yii::t('yii', 'Update'),
                        'aria-label' => Yii::t('yii', 'Update'),
                        'data-pjax' => '0',
                    ],
                    $this->buttonOptions
                );
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['delete'])) {
            $this->buttons['delete'] = function ($url, $model, $key) {
                $options = array_merge(
                    [
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ],
                    $this->buttonOptions
                );
                Html::removeCssClass($options, 'btn-default');
                Html::addCssClass($options, 'btn-danger');
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['softdelete'])) {
            $this->buttons['softdelete'] = function ($url, $model, $key) {
                $url = Url::toRoute(['soft-delete','id'=>$model->id]);
                $options = array_merge(
                    [
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ],
                    $this->buttonOptions
                );
                Html::removeCssClass($options, 'btn-default');
                Html::addCssClass($options, 'btn-danger');
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
            };
        }
    }

}